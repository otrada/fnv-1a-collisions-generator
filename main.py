from liblll import create_matrix_from_knapsack, lll_reduction
import random

p = pow(2, 128)
h0 = 144066263297769815596495629667062367629
g = pow(2, 88) + 315


COLLISIONS = []
MIN_LEN = 10
MAX_LEN = 20


def fnv1a(arr):
    h = h0
    for i in arr:
        h = ((i ^ h) * g) % p
    return h


def hash_first(vec):
    h = h0
    for i in vec:
        h = ((i + h) * g) % p
    return h


def change_hash_type(vec):
    h = h0
    v = []
    for i in vec:
        n = (i + h) ^ h
        if n > 256:
            return False
        v.append(n)
        h = ((i + h) * g) % p
    return v


def gen_matrix():
    r = random.randint(MIN_LEN, MAX_LEN)
    sum_ = random.randint(0, p) + 15 * p
    vect = [pow(g, i, p) for i in range(1, r + 1)] + [p]
    matr = create_matrix_from_knapsack(vect, sum_)
    return lll_reduction(matr)


def extract_arrays_from_matrix(matrix):
    ar = []
    hs = []

    for i in range(len(matrix[0])):
        v = []
        for j in range(len(matrix) - 2):
            v.append(int(matrix[j][i]))
        v = v[::-1]
        ar.append(v)
        hs.append(hash_first(v))
    return check_vectors(ar, hs)

def check_vectors(ar, hs):
    dic = {}
    for i in range(len(hs)):
        if hs.count(hs[i]) > 1:
            if hs[i] in dic:
                dic[hs[i]].append(ar[i])
            else:
                dic[hs[i]] = [ar[i]]
    return dic


def format_vector(vec):
    s = ""
    for i in vec:
        s += chr(i)
    return s.encode("hex")


def attack():
    while True:
        m = gen_matrix()
        dic = extract_arrays_from_matrix(m)
        for key in dic:
            ctr = 0
            colls = []
            hs = []
            for i in dic[key]:
                hs.append(hash_first(i))
            for i in dic[key]:
                c = change_hash_type(i)
                if c != False:
                    ctr += 1
                    colls.append(c)
            if ctr > 1:
                for i in colls:
                    if i not in COLLISIONS:
                        print fnv1a(i), format_vector(i)
                        COLLISIONS.append(i)

attack()